module.exports = {
  reactStrictMode: true,
  images: {
    domains: ["images.unsplash.com", "dummyimage.com","www.designspiration.com"],
  },
}
