import React from 'react'
import {sanityClient, urlFor} from "../sanity";
import Link from "next/link"

function news({posts}) {
    return (
        <div>
<h2 className="text-5xl my-20 font-extrabold sm:text-4xl text-center">Blog</h2>
<div className="max-w-7xl bg-white p-12">
<div className="grid  grid-cols-1 sm:grid-cols-2 lg:grid-cols-3 gap-3 md:gap-6 p-2 md:p-6 ">
       {posts.map((post) => (
         <Link key={post._id} href={`/${post.slug.current}`}>
           <div className="border rounded-lg group cursor-pointer overflow-hidden">
             <img className="h-60 w-full object-cover group-hover:scale-105 transition-transform duration-200 ease-in-out" src={
              urlFor(post.mainImage).url()
             } alt="" />
             <div className="flex justify-between p-5 bg-white">
               <div>
                 <p className="text-lg font-bold ">{post.title}</p>
                 <p className="text-xs ">{post.description} by </p>
               </div>

               <img className="h-12 w-12 rounded-full" src="" alt="" />
             </div>
           </div>
         </Link>
       ))}
     </div>
    </div>
        </div>
    )
}

export default news

export const getServerSideProps = async () => {
    const query = `*[_type == "post"] {
      _id,
      title,
      slug,
      author -> {
      name,
      image
    },
      description,
      mainImage,
      slug
    
    }`;
    const posts = await sanityClient.fetch(query);
  
    return {
      props: {
        posts,
      },
    }
  }