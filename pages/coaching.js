import React from 'react'

function coaching() {
    return (
        <div>
                  <div className="max-w-lg sm:text-center sm:mx-auto ">
                  
            <h2 className="mb-6 font-sans mt-20 text-3xl font-bold tracking-tight text-gray-900 sm:text-4xl sm:leading-none">
                <span className="relative inline-block">
                    <svg
                        viewBox="0 0 52 24"
                        fill="currentColor"
                        className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-blue-gray-100 lg:w-32 lg:-ml-32 lg:-mt-10 sm:block"
                    >
                        <defs>
                            <pattern
                                id="6b0188f3-b7a1-4e9b-b95e-cad916bb3042"
                                x="0"
                                y="0"
                                width=".135"
                                height=".30"
                            >
                                <circle cx="1" cy="1" r=".7" />
                            </pattern>
                        </defs>
                        <rect
                            fill="url(#6b0188f3-b7a1-4e9b-b95e-cad916bb3042)"
                            width="52"
                            height="24"
                        />
                    </svg>
                    <span className="relative bg-white">Les</span>
                </span>{' '}
                Coachings           </h2>
                  </div>

          

            {/* Section Coaching Individuel */}
            <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                <div className="flex flex-col max-w-screen-lg overflow-hidden bg-white border rounded shadow-sm lg:flex-row sm:mx-auto">
                    <div className="relative lg:w-1/2">
                        <img
                            src="https://images.unsplash.com/photo-1579835017831-564402efad0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NzR8fHB1cnBsZSUyMGZsb3dlcnxlbnwwfDF8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            alt=""
                            className="object-cover hidden md:block w-full lg:absolute h-80 lg:h-full"
                        />
                        <svg
                            className="absolute top-0 right-0 hidden h-full text-white lg:inline-block"
                            viewBox="0 0 20 104"
                            fill="currentColor"
                        >
                            <polygon points="17.3036738 5.68434189e-14 20 5.68434189e-14 20 104 0.824555778 104" />
                        </svg>
                    </div>
                    <div className="flex flex-col justify-center p-8 bg-white lg:p-16 lg:pl-10 lg:w-1/2">
                        <div>
                            <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                                Brand new
                            </p>
                        </div>
                        <h5 className="mb-3 text-3xl font-extrabold leading-none sm:text-4xl">
                            Coaching individuel          </h5>
                        <p className="mb-5 text-gray-800">
                            <span className="font-bold">Par mon écoute attentive </span> et mon questionnement adapté je t’accompagne dans tes réflexions et prises de conscience à comprendre où tu en es actuellement.
                            Le but étant d’améliorer l'efficacité de ta communication, d’améliorer tes relations interpersonnelles (sociales, de couple, parent/enfant, etc.), mais aussi
                            retrouver une estime de soi et affirmer ton  identité.

                            Un coaching personnel repose sur un plan d’action concret, que nous définissons ensemble.

                            Je m’appuie pour cela, sur une multitude d’outils, et notamment, les suivants, que j’affectionne particulièrement. (L’ennéagramme, l’analyse transactionnelle, et évidemment la pnl !)
                            Cette large grille de lecture me permet d’enrichir mes accompagnements, mais surtout d’affiner mon expertise.

                        </p>
                        <div className="flex items-center">
                            <a
                                href="/"
                                aria-label=""
                                className="inline-flex items-center font-semibold transition-colors duration-200  hover:text-blueRosa"
                            >
                                Réserve ta séance découverte pour en discuter !
                                <svg
                                    className="inline-block w-3 ml-2"
                                    fill="currentColor"
                                    viewBox="0 0 12 12"
                                >
                                    <path d="M9.707,5.293l-5-5A1,1,0,0,0,3.293,1.707L7.586,6,3.293,10.293a1,1,0,1,0,1.414,1.414l5-5A1,1,0,0,0,9.707,5.293Z" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>

            <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
                <div className="flex flex-col max-w-screen-lg overflow-hidden bg-white border rounded shadow-sm lg:flex-row sm:mx-auto">
                    <div className="relative lg:w-1/2">
                        <img
                            src="https://images.unsplash.com/photo-1579835017831-564402efad0d?ixid=MnwxMjA3fDB8MHxzZWFyY2h8NzR8fHB1cnBsZSUyMGZsb3dlcnxlbnwwfDF8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
                            alt=""
                            className="object-cover hidden md:block w-full lg:absolute h-80 lg:h-full"
                        />
                        <svg
                            className="absolute top-0 right-0 hidden h-full text-white lg:inline-block"
                            viewBox="0 0 20 104"
                            fill="currentColor"
                        >
                            <polygon points="17.3036738 5.68434189e-14 20 5.68434189e-14 20 104 0.824555778 104" />
                        </svg>
                    </div>
                    <div className="flex flex-col justify-center p-8 bg-white lg:p-16 lg:pl-10 lg:w-1/2">
                        <div>
                            <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-teal-900 uppercase rounded-full bg-teal-accent-400">
                                Brand new
                            </p>
                        </div>
                        <h5 className="mb-3 text-3xl font-extrabold leading-none sm:text-4xl">
                            Coaching collectif          </h5>
                        <p className="mb-5 text-gray-800">
                            <span className="font-bold">J’anime des ateliers collectifs entre femmes  </span> 2 fois par mois. (en présentiel à Lyon ).
                            Nous abordons à chaque fois, une thématique différente de manière ludique et thérapeutique. Je profite de la dynamique de groupe, pour solliciter l’intelligence collective et ainsi enrichir les interactions. Tu apprendras beaucoup sur toi , mais aussi des autres.
                            Tolérance et bienveillance sont les seuls mots d’ordre !
                            Chaque 1er du mois, tu recevras les dates des ateliers du mois suivant.


                        </p>
                        <div className="flex items-center">
                            <a
                                href="/"
                                aria-label=""
                                className="inline-flex items-center font-semibold transition-colors duration-200  hover:text-blueRosa"
                            >
                                Inscris-toi vite, pour rester informée !
                                <svg
                                    className="inline-block w-3 ml-2"
                                    fill="currentColor"
                                    viewBox="0 0 12 12"
                                >
                                    <path d="M9.707,5.293l-5-5A1,1,0,0,0,3.293,1.707L7.586,6,3.293,10.293a1,1,0,1,0,1.414,1.414l5-5A1,1,0,0,0,9.707,5.293Z" />
                                </svg>
                            </a>
                        </div>
                    </div>
                </div>
            </div>




            <section className="bg-paiement bg-cover px-5  py-10">
                <div className="grid grid-cols-1 gap-8 mt-8 md:grid-cols-2 lg:grid-cols-3 justify-items-center flex-col lg:flex-row">
                    <div class="shadow-2xl rounded-3xl w-96 h-72">
                        <div class="p-8 text-center sm:p-12">
                            <p class="text-lg font-semibold tracking-widest text-purpleRosa uppercase">COACHING PERSONNEL</p>

                            <h5 class="mt-6 text-3xl font-bold">590 €</h5>

                            <a class="inline-block w-full py-4 mt-8 text-sm font-bold text-white hover:text-blueRosa bg-purpleRosa rounded-full shadow-xl" href="">
                                Réserver
                            </a>
                        </div>
                    </div>

                    <div class="shadow-2xl rounded-3xl w-96 h-72">
                        <div class="p-8 text-center sm:p-12">
                            <p class="text-lg font-semibold tracking-widest text-purpleRosa  uppercase">COACHING PROFESSIONNEL</p>

                            <h5 class="mt-6 text-3xl font-bold">690 €</h5>

                            <a class="inline-block w-full py-4 mt-8 text-sm font-bold text-white hover:text-blueRosa bg-purpleRosa rounded-full shadow-xl" href="">
                                Réserver
                            </a>
                        </div>
                    </div>

                    <div class="shadow-2xl rounded-3xl w-96 h-72">
                        <div class="p-8 text-center sm:p-12">
                            <p class="text-lg font-semibold tracking-widest text-purpleRosa uppercase">ATELIERS COLLECTIFS</p>

                            <h5 class="mt-6 text-3xl font-bold">45 €</h5>
                            <a class="inline-block w-full py-4 mt-8 text-sm font-bold text-white bg-purpleRosa hover:text-blueRosa rounded-full shadow-xl" href="">
                                Réserver
                            </a>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    )
}

export default coaching
