import Footer from '../components/Footer'
import Header from '../components/Header'
import NavBar from '../components/NavBar'
import '../styles/globals.css'
import '../styles/nav.css'
function MyApp({ Component, pageProps }) {
  return(
<>
 <Header/>
  <NavBar/>  
  <Component {...pageProps} />
   <Footer/>
    </>
  ) 
}

export default MyApp
