import React from 'react'
import Image from 'next/image'
import Header from '../components/Header'
import NavBar from '../components/NavBar'
import Footer from '../components/Footer'
function about() {
    return (
        <div>
         
            <section className="flex lg:px-10 mx-10">
                <div class="flex flex-col lg:flex-row lg:space-x-20 lg:mx-20">
                    <div class="w-full lg:w-1/2 lg:space-y-5 mt-10">
                        <img src="/rosa.jpg"></img>
                    </div>
                    <div class="w-full lg:w-1/2 text-justify space-y-5 mt-10">
                        <h2 className="font-serif font-bold text-3xl">Hello à toutes ! </h2>
                        <h1 className="font-bold text-5xl text-purpleRosa uppercase">Je m'appelle Rosa</h1>
                        <h2 className="font-serif text-3xl">Je suis coach  professionnelle certifiée et praticienne en PNL</h2>
                        <p>Je t'aide à découvrir ton fonctionnement et à comprendre tes
                            mécanismes inconscients.
                            Mon rôle est de t'accompagner dans tes réflexions, te proposer
                            des outils efficaces, ainsi que des actions concrètes, pour te
                            permettre d'atteindre tes objectifs rapidement.
                        </p>
                    </div>
                </div>
            </section>





            <section>
                <div>
                    <h1 className="font-serif font-bold text-center text-purpleRosa text-4xl mt-20">Pourquoi ma vocation est-elle de t'aider à créer la tienne ?</h1>
                </div>
                <div className="flex flex-col lg:flex-row lg:space-x-20 mx-10">
                    <div className="w-full lg:w-1/2 text-justify space-y-5 mt-10">
                        <p>Si je veux autant t’aider à trouver ta voie, c’est bien parce que j’ai beaucoup souffert de ne pas en avoir une moi-même. Je n’ai jamais rêvé d’être médecin comme certaines de mes copines. Je n’ai pas non plus supporté de suivre une voie toute tracée recommandée pour les « hauts potentiels » en prépa puis en grande entreprise. Je n’avais pas non plus envie de faire de l’humanitaire comme le suggérait ma conseillère d’orientation à tous ceux qui voulaient être utiles. Bref, je savais ce que je ne voulais pas, mais je ne savais pas ce que je voulais.</p>
                        <p>Alors j’ai cherché ! J’ai testé plusieurs expériences dans différentes structures et dans divers secteurs pour identifier ce que j’aimais faire, avec qui j’aimais travailler et selon quel mode de fonctionnement. J’ai commencé à trouver beaucoup de sens à mes activités en découvrant le secteur de l’éducation. Je l’ai exploré en long en large et en travers durant mes stages. D’abord dans la formation au sein de l’ONG israélienne WePower, puis dans le conseil en stratégie chez Headway Advisory et enfin à la coopération universitaire de l’Ambassade de France aux Etats-Unis.</p>
                        <p>Une fois mon secteur de prédilection identifié, j’ai réalisé que la posture qui me correspondait le mieux était une posture </p>
                    </div>
                    <div className="w-full lg:w-1/2 text-justify space-y-5 mt-10">
                        <p>entrepreneuriale. Je l’ai d’abord apprivoisé en co-fondant et développant mes propres associations du NOISE et de Coexister Paris sur mon temps libre. En parallèle, j’entreprenais au sein d’organisations existantes dans la start-up sociale Ticket for Change puis dans l’école de commerce ESCP Europe. Je pensais que ce mode de vie de « slasheuse » me correspondait : travailler pour des projets le jour et créer les miens le soir. Mais au bout d’un moment, ce mode de vie ne me correspondait plus. Il m’épuisait.</p>
                        <p>Sauf que je ne pensais pas être capable de vivre de mes projets. Je n’avais pas d’équipe, je n’avais pas d’idée et je n’avais pas de liberté financière. Heureusement, j’avais un coach ! Il m’a aidé à prendre conscience que je n’étais plus heureuse dans mon poste salarié et que j’aspirais à devenir coach à mon tour. Il m’a aidé à prendre confiance en moi et à développer les capacités nécessaires pour changer ma vie sereinement. J’ai préparé ma transition progressivement pour avancer pas à pas vers mon nouvel idéal de vie. Ma certification de coach et formatrice à l’Institut des Neurosciences Motivationnelles m’a confirmé à quel point j’étais passionnée d’aider les gens à développer leur potentiel (et ma mention très bien en témoigne ;))</p>
                    </div>
                </div>
            </section>

            <section className="lg:flex my-20 mx-5">
                <div className="flex lg:justify-start justify-center lg:ml-80">
                    <img className="h-screen"
                        src="https://images.unsplash.com/photo-1605635496372-3ac60aebe7a5?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OTF8fHB1cnBsZXxlbnwwfDF8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"></img>
                </div>
                <div className="flex lg:justify-center lg:ml-30">
                    <div className="bg-beige lg:p-5 lg:mx-10 flex flex-col justify-center  lg:absolute p-5   mt-20  lg:w-1/3">

                        <h1 className="font-bold font-serif text-4xl text-center">La coach des femmes entreprenantes en quête de vocations professionnelles épanouissantes et impactantes !
                        </h1>
                        <p className="my-5">Lorem ipsum dolor sit amet. Qui nulla quia qui dolores quia non eveniet animi non laboriosam quod! Sit quos exercitationem sit modi quaerat et magni consequatur non inventore numquam et aspernatur eveniet sed asperiores nulla eos nobis autem. Vel possimus consequatur aut nostrum doloribus sed quae omnis quo quod sequi quo odio alias et dolorum veritatis?


                            In quis corrupti ut quasi obcaecati ut vitae molestiae in consequatur alias et sequi aliquam. Eos consequatur ullam non tenetur distinctio dicta ullam aut tempore voluptate est ipsum tempora.</p>
                        <div className="text-center mt-5">
                            <button className="uppercase hidden bg-purpleRosa text-white hover:bg-blueRosa p-3 mb-5">
                                Découvrir
                            </button>
                        </div>
                    </div>
                </div>

            </section>



            {/* <section className="my-20 flex flex-col justify-center ">
                <div className=" w-full h-1/2 bg-about bg-no-repeat bg-cover lg:w-3/4 ">
                    <h1 className="font-serif font-bold text-white text-4xl text-center">Tu te sens un peu perdue dans ta vie pro
                        et tu souhaiterais y retrouver du sens et de l’équilibre
                        pour te sentir plus épanouie et utile aux autres ?</h1>

                    <div className="text-center mt-5">
                        <button className="uppercase bg-purpleRosa text-white hover:bg-blueRosa p-3 mb-5">
                            En savoir plus sur moi
                        </button>
                    </div>
                </div>


            </section> */}
           
        </div>
    )
}

export default about
{/* <section className="md:flex mx-20 items-center"> 
<div className="w-full lg:w-full "> 
    <img src="https://images.unsplash.com/photo-1540324155974-7523202daa3f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Njd8fHdvbWFufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
    
    />
</div>
<div className="mx-10"> 
    <h2 className="font-serif font-bold text-3xl">Bonjour à toutes ! </h2>
    <h1 className="font-bold text-5xl text-pink-600 uppercase">Je m'appelle Rosa</h1>
    <h2 className="font-serif text-3xl">Je suis coach conférencière et auteure</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
    
    </p>
</div>
</section>

<section className="md:flex items-center">
<div className="h-100 w-100 lg:w-full "> 
    <img src="https://images.unsplash.com/photo-1540324155974-7523202daa3f?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Njd8fHdvbWFufGVufDB8fDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
    
    />
</div>
<div className="mx-10"> 
    <h2 className="font-serif font-bold text-3xl">Bonjour à toutes ! </h2>
    <h1 className="font-bold text-5xl text-pink-600 uppercase">Je m'appelle Rosa</h1>
    <h2 className="font-serif text-3xl">Je suis coach conférencière et auteure</h2>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborumLorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
    
    </p>
</div>
</section> */}