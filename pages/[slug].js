import React from 'react'
import PortableText from 'react-portable-text'
import { sanityClient, urlFor } from '../sanity'

function Post({ post }) {
    return (
        <main>
            <img src={urlFor(post.mainImage).url()} 
            className="w-full h-70 object-contain md:w-1/2 mt-10 mx-auto" alt="" />
            <article className="max-w-3xl mx-auto p-5">
            <h1 className="text-3xl mt-10 mb-3">{post.title}</h1>
                <h2 className="text-xl font-light text-gray-500 mb-2">
                    {post.description}
                </h2>
                <div className="flex items-center space-x-2">
                    <img className="h-10 w-10 rounded-full"
                        src="" alt="" />
                    <p className="font-extralight text-sm">
                        Blog post by {" "}
                        <span className="text-green-600"></span>
                        - Published at {" "}
                        {new Date(post._createdAt).toLocaleString()}
                    </p>
                </div>

                <div className="mt-10">
                    <PortableText
                        dataset={process.env.NEXT_PUBLIC_SANITY_DATABASE}
                        projectId={process.env.NEXT_PUBLIC_SANITY_PROJECT_ID}
                        content={post.body}
                        serializers={{
                            h1: (props) => (
                                <h1 className="text-2xl font-bold my-5" {...props} />
                            ),
                            h2: (props) => (
                                <h1 className="text-xl font-bold my-5" {...props} />
                            ),
                            li: ({ children }) => (
                                <li className="ml-4 list-disc">{children}</li>
                            ),
                            link: ({ href, children }) => (
                                <a href={href} className="text-blue-500 hover:underline">
                                    {children}
                                </a>
                            ),
                        }}
                    />
                </div>

            </article>
        </main>
    )
}

export default Post

export const getStaticPaths = async () => {
    const query = `*[_type == "post"] {
    _id,
    slug {
        current
    }
}`;

    const posts = await sanityClient.fetch(query);

    const paths = posts.map((post) => ({
        params: {
            slug: post.slug.current
        }
    }));

    return {
        paths,
        fallback: 'blocking'
    }
}

export const getStaticProps = async ({ params }) => {
    const query = `*[_type == "post" && slug.current == $slug] [0] {
    _id,
    _createdAt,
    title,
    author-> {
        name,
        image
    },
    'comments': *[
        _type == "comment" &&
        post._ref == ^._id &&
        approved == true],
        description, 
        mainImage,
        slug,
        body
    
}`

    const post = await sanityClient.fetch(query, {
        slug: params?.slug

    })

    if (!post) {
        return {
            notFound: true
        }
    }

    return {
        props: {
            post,
        },
        revalidate: 60, // after 60seconds, itll update the old cached version
    }
}