import React from 'react'
import Header from '../components/Header'
import NavBar from '../components/NavBar'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faInstagram, faLinkedin, faPinterest, faYoutube } from '@fortawesome/free-brands-svg-icons';
import ContactForm from '../components/ContactForm';
import Footer from '../components/Footer';

function contact() {
    return (
        <div>
          
            <section className="">
                <div className="bg-contact lg:p-64 items-center md:p-28 bg-cover lg:relative ">
                    <div className="bg-white  py-20 px-10 mt-5 rounded-lg">
                        <h2 className="uppercase text-5xl font-serif text-center mb-5 font-bold text-purpleRosa ">à bientôt !</h2>
                        <p className="flex-wrap text-center">Merci beaucoup pour ta visite sur mon site ! J’espère que tu y as trouvé ton bonheur. J’adore me connecter avec d’autres personnes sur la même longueur d’onde que moi.</p>

                        <div className="flex space-x-8 justify-center mr-8 mt-3 text-purpleRosa ">

                            <a href="https://www.facebook.com/wardya.sekkour.3"><FontAwesomeIcon className="hover:text-blueRosa cursor-pointer" icon={faFacebook} style={{ width: '40px' }} /></a>
                            <a href="https://www.linkedin.com/in/rosa-ghozali"><FontAwesomeIcon className="hover:text-blueRosa cursor-pointer" icon={faLinkedin} style={{ width: '40px' }} /></a>
                           <a href="https://www.instagram.com/les_ateliers_de_rosa/"><FontAwesomeIcon className="hover:text-blueRosa cursor-pointer" icon={faInstagram} style={{ width: '40px' }} /></a> 
                           

                        </div>
                    </div>
                </div>
            </section>

            

            <section className="bg-beige mt-20">
                
                <ContactForm/>


                
            </section>
 
        </div>
    )
}

export default contact
{/*  */}