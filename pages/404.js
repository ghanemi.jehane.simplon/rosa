import React from 'react'

function NotFound() {
    return (

        <div class="">
            <div className="flex justify-center mx-auto">
            <img
                src="https://media.giphy.com/media/xT9IgHKDptUFWWMkve/giphy.gif"
                alt="Purple Rosa lost"
                class="object-cover w-full md:w-1/2  h-64 rounded-lg"
            />
            </div>
       
            <div class="text-center">
                <p class="mt-6 text-gray-500">We can't find anything, try searching again.</p>
            </div>

        </div>
    )
}

export default NotFound
