const colors = require('tailwindcss/colors')
module.exports = {
  purge: [],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      backgroundImage: {
        'jumbotron': "url('https://images.unsplash.com/photo-1600759487717-62bbb608106e?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8bGF2ZW5kZXIlMjBmaWVsZHxlbnwwfHwwfHw%3D&auto=format&fit=crop&w=500&q=60')",
        'second': "url('https://images.unsplash.com/photo-1500904156668-758cff89dcff?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1170&q=80')",
        'contact': "url('https://images.unsplash.com/photo-1558481795-7f0a7c906f5e?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1196&q=80')",
        'about' : "url('https://images.unsplash.com/photo-1534611681095-5544f1850c22?ixid=MnwxMjA3fDB8MHxzZWFyY2h8OHx8bGF2YW5kJTIwZmllbGRzfGVufDB8MHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60')",
        'paiement': "url('https://images.unsplash.com/photo-1519751138087-5bf79df62d5b?ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8bGlnaHR8ZW58MHwwfDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60')"
      },
      fontFamily: {
        NotoSerif: ['Noto Serif', 'serif'],
       },
    },
    colors:{
      white: colors.white,
      gray: colors.gray,
      blueRosa: '#6cbfd1',
      purpleRosa: '#5a1d80',
      pink: colors.pink,
      yellow: colors.yellow,
      beige: '#F8F4F0',
      green: colors.emerald,
      yellow: colors.amber,
      purple: colors.violet,

    }
    
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
