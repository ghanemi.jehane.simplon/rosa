import React from 'react'
import Image from 'next/image'
import SwiperCore, { Pagination, Autoplay, Navigation, Scrollbar, A11y, EffectCoverflow } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { sanityClient, urlFor } from '../sanity';
import Link from 'next/link'




function Cards({ posts }) {
    console.log('posts', posts);
    return (
        <div className="mt-20 mb-20">
            <div>
                <h2 className="text-3xl  font-bold text-center text-gray-600 mb-10">Les inspirations de mon blog</h2>
            </div>
            <Swiper
                modules={[Autoplay, Navigation, Pagination, Scrollbar, A11y, EffectCoverflow]}

                spaceBetween={50}
                slidesPerView={'auto'}
                centeredSlides={true}
                autoplay={{
                    delay: 3000,
                    disableOnInteraction: false,
                }}
                coverflowEffect={{
                    rotate: 50,
                    stretch: 0,
                    depth: 100,
                    modifier: 1,
                    slideShadows: true,
                }}
                pagination={{ clickable: true }}
                loop={true}
                className="p-3 max-w-screen-lg text-center">

                {posts.map((post) => (
                    <SwiperSlide key={post._id}>
                        <div className="flex flex-col w-72 space-y-3 items-center p-4 border sm:p-6 rounded-xl">
                            <Link href={`/${post.slug.current}`}>
                                <img
                                    src={urlFor(post.mainImage).url()}
                                    className="h-60 object-cover group-hover:scale-105 transition-transform duration-200 ease-in-out "
                                />
                            </Link>
                            <div>
                                <h2 className="mt-4 text-2xl font-semibold text-gray-700 capitalize dark:text-white">{post.title}</h2>
                                <p className="mt-2 text-gray-500 capitalize dark:text-gray-300">{post.description}</p>
                            </div>
                            <Link href={`/${post.slug.current}`}>
                                <button class="px-4 py-2 font-medium tracking-wide text-white capitalize transition-colors duration-200 transform bg-blueRosa rounded-md hover:bg-blue-500 focus:outline-none focus:ring focus:ring-blue-300 focus:ring-opacity-80">
                                    Lire plus                                
                                </button>
                                </Link>
                        </div>

                    </SwiperSlide>


                ))}




            </Swiper>

        </div>
    )
}

export default Cards
