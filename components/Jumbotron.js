import React from 'react'
function Jumbotron() {
  return (
    <div className="flex flex-col md:text-left bg-jumbotron bg-cover w-full h-screen lg:h-[600px] xl:h-[700px] xl:w-full ">
      <h1 className="text-4xl px-10 lg:text-5xl flex-wrap font-bold text-center mt-20">Sors de tes méconnaissances et reprends le pouvoir sur ta vie!</h1>
    <h2 className="text-3xl px-10 lg:text-4xl mx-auto mt-20 text-center text-white ">J’aide les femmes à se re-découvrir et à apprendre à utiliser leur propre ressource au profit de leur épanouissement !</h2>
    </div>

  )
}

export default Jumbotron

// <div className="bg-jumbotron h-screen">
//                <div className="container flex-grow justify-end">
//         <h1 className="text-3xl text-white font-large title-font mb-2 mr-10 ml-10">Deviens entrepreneuse de ta vie en créant un parcours à ton image !</h1>
//       <p className="leading-relaxed text-white ml-10">J’aide les femmes entreprenantes et engagées comme moi à construire une carrière épanouissante et impactante ! Et si toi aussi tu faisais de ta singularité une force pour trouver ta place dans le monde du travail ?</p>
//        <button className="uppercase bg-pink-500 text-white hover:bg-yellow-500 ml-10 mt-5 p-5">Mes 3 vidéos pour te guider</button>
//        </div>
//         </div>