import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebook, faInstagram, faLinkedin, faYoutube } from '@fortawesome/free-brands-svg-icons';


function Footer() {
    return (
        <div className="mt-20 p-10 bg-beige">
            
            
            <div className="flex space-x-8 justify-center mr-8 mt-10 mb-10 text-blueRosa ">
            
            <a target="_blank"  href="https://www.facebook.com/wardya.sekkour.3"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faFacebook} style={{width:'25px'}}/></a>
            <a target="_blank" href="https://www.linkedin.com/in/rosa-ghozali"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faLinkedin} style={{width:'25px'}}/></a>
            <a target="_blank" href="https://www.instagram.com/les_ateliers_de_rosa/"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faInstagram} style={{width:'25px'}}/></a> 
        </div>


        <div className="flex flex-wrap justify-center text-gray-600 space-x-4 mb-5">
            <p>@2021 Rosa Ghozali -</p>
            <p>Tous droits réservés </p>
            <p className="text-purpleRosa hover:text-blueRosa cursor-pointer">Mentions Légales </p>
            <p className="text-purpleRosa hover:text-blueRosa cursor-pointer">Politique de Confidentialité</p>
            <p className="text-purpleRosa hover:text-blueRosa cursor-pointer">Conditions Générales de Vente</p>
        </div>
        </div>
    )
}

export default Footer
