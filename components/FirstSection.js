import React from 'react'
import Image from 'next/image'
import { ChevronDoubleRightIcon } from '@heroicons/react/solid'

function FirstSection() {
    return (
       <div>

<div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
      <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
        <div>
          <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-blueRosa uppercase rounded-full bg-teal-accent-400">
            Brand new
          </p>
        </div>
        <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
          <span className="relative inline-block">
            <svg
              viewBox="0 0 52 24"
              fill="currentColor"
              className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-blueRosa lg:w-32 lg:-ml-28 lg:-mt-10 sm:block"
            >
              <defs>
                <pattern
                  id="07690130-d013-42bc-83f4-90de7ac68f76"
                  x="0"
                  y="0"
                  width=".135"
                  height=".30"
                >
                  <circle cx="1" cy="1" r=".7" />
                </pattern>
              </defs>
              <rect
                fill="url(#07690130-d013-42bc-83f4-90de7ac68f76)"
                width="52"
                height="24"
              />
            </svg>
            <span className="relative">Tu</span>
          </span>{' '}
          as du mal à te réinventer ?
        </h2>
        <p className="text-base text-gray-700 md:text-lg">
        Tu essaies de prendre du temps pour toi, parce que tu en as besoin, mais le problème c’est que:
        </p>
      </div>
      <div className="grid max-w-screen-lg mx-auto space-y-6 lg:grid-cols-2 lg:space-y-0 lg:divide-x">
        <div className="space-y-6 sm:px-16">
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                The deep ocean
              </h6>
              <p className="text-sm text-gray-900">
              Tu programmes une sortie entre filles, c'est sympa, tu t'amuses mais ça ne règle pas le problème !
              </p>
            </div>
          </div>
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                When has justice
              </h6>
              <p className="text-sm text-gray-900">
              Quoi que tu fasses avec tes proches, tu ressens que quelque chose ne te convient pas, comme si tu n'étais pas à ta place, ou bien qu'ils ne te comprenaient pas
              </p>
            </div>
          </div>
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                Leverage agile
              </h6>
              <p className="text-sm text-gray-900">
              Tu t'offres un massage histoire de décompresser ça fait un bien fou, mais après une semaine, rebelotte, la mélancolie revient. Pourquoi ?
              </p>
            </div>
          </div>
        </div>
        <div className="space-y-6 sm:px-16">
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                Organically grow
              </h6>
              <p className="text-sm text-gray-900">
              Parce que tu n'as pas identifié ce dont tu as besoin au plus profond de toi ! Tu mets 
            des petits pansements régulièrement en espérant que cela passe mais tu ne fais que gagner du temps !
              </p>
            </div>
          </div>
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                Have a good one
              </h6>
              <p className="text-sm text-gray-900">
                Cheese on toast airedale the big cheese. Danish fontina cheesy
                grin airedale danish fontina.
              </p>
            </div>
          </div>
          <div className="flex flex-col max-w-md sm:flex-row">
            <div className="mb-4 mr-4">
              <div className="flex items-center justify-center w-12 h-12 rounded-full bg-indigo-50">
                <svg
                  className="w-8 h-8 text-purpleRosa sm:w-10 sm:h-10"
                  stroke="currentColor"
                  viewBox="0 0 52 52"
                >
                  <polygon
                    strokeWidth="3"
                    strokeLinecap="round"
                    strokeLinejoin="round"
                    fill="none"
                    points="29 13 14 29 25 29 23 39 38 23 27 23"
                  />
                </svg>
              </div>
            </div>
            <div>
              <h6 className="mb-3 text-xl font-bold leading-5">
                A slice of heaven
              </h6>
              <p className="text-sm text-gray-900">
                A flower in my garden, a mystery in my panties. Heart attack
                never stopped old Big Bear.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>


         {/* <section className="lg:flex my-20">
             <div className="flex lg:justify-start justify-center lg:ml-80">
                <img className="h-1/4 lg:h-screen hidden lg:block w-full lg:w-96 "
                    src="https://images.unsplash.com/photo-1517504734587-2890819debab?ixid=MnwxMjA3fDB8MHxzZWFyY2h8M3x8cHVycGxlfGVufDB8fDB8fA%3D%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"></img>
            </div>
            <div className="flex lg:justify-center lg:ml-48">
            <div className="bg-beige p-5 mx-10 flex flex-col justify-center  lg:absolute   mt-20 w-full  lg:w-96">
                
                <h1 className="font-bold font-serif text-4xl text-center">Tu as du mal à te réinventer ?</h1>
                <p>Tu essaies de prendre du temps pour toi, parce que tu en as besoin, mais le problème c’est que :

</p>
                <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5" />
        <p>Tu programmes une sortie entre filles, c'est sympa, tu t'amuses mais ça ne règle pas le problème !</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5" />
        <p>Quoi que tu fasses avec tes proches, tu ressens que quelque chose ne te convient pas, comme si tu n'étais pas à ta place, ou bien qu'ils ne te comprenaient pas.</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5" />
        <p>Tu t'offres un massage histoire de décompresser ça fait un bien fou, mais après une semaine, rebelotte, la mélancolie revient. Pourquoi ?</p>
    </div>
    <div className="flex text-center mt-5 font-semibold">
        <p>Parce que tu n'as pas identifié ce dont tu as besoin au plus profond de toi ! Tu mets 
            des petits pansements régulièrement en espérant que cela passe mais tu ne fais que gagner du temps !
        </p>
    </div>
    <div className="flex text-center mt-5 font-semibold">
        <p>Alors si tu es prête à t'investir pour que le travail que nous accomplirons ensemble, te permette de:
        </p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5" />
        <p>Récupérer la confiance en toi, et l'estime que tu as pour toi.</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5" />
        <p>Découvrir les mécanismes de ton propre fonctionnement pour enfin, reprendre les commandes de ta vie.</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blueRosa h-5 font-bold" />
        <p>Puisez dans tes ressources, pour te donner la force de vivre la vie que tu souhaites.</p>
    </div>
    <div className="text-center mt-5 mb-5">
        <button className="uppercase hidden bg-purpleRosa text-white hover:bg-blueRosa p-3 mb-5">
            Ne réfléchis plus réserves ta séance découverte !
        </button>
    </div>
            </div>
            </div>
            
            </section> */}
           
       </div>
    )
}

export default FirstSection
{/*  <div className=""> 
           <div className="mt-10 flex justify-items-center container  lg:m-52">
<div>
    <Image src="https://images.unsplash.com/photo-1597618711729-aaea2fd0c63a?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mjl8fGdyYWZmaXRpfGVufDB8MXwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=500&q=60"
        width="600" height="800" className="relative flex-grow" /></div>
<div className="bg-white shadow-lg lg:absolute ml-96  mt-32 pt-10 pl-4 p-5 flex-grow w-full sm:w-2/5 md:2/5 lg:2/5 ">
    <h2 className="text-4xl text-justify ">Tu as du mal à te réinventer ?</h2>
    <p className="text-justify mt-5">Tu es prête à t’investir pour que ton travail soit à la fois un outil d’épanouissement personnel et de contribution positive au monde. Le problème c’est que :</p>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blue-500 h-5" />
        <p>Tu ne sais plus comment t'y prendre pour construire la prochaine étape de ton parcours</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blue-500 h-5" />
        <p>Tu sens que tu as identifié des envies mais tu ne vois pas bien comment vivre sereinement de tes passions</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blue-500 h-5" />
        <p>Tu manques de confiance en toi et tu crains la critique et le regard des autres</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blue-500 h-5" />
        <p>Tu rêves de plus de liberté ou d’entreprendre mais tu as peur de changer (de job)</p>
    </div>
    <div className="flex mt-5">
        <ChevronDoubleRightIcon className="text-blue-500 h-5" />
        <p>Tu te sens débordée car tu as du mal à faire des choix, à dire non et à fixer des limites. Tu es épuisée de tout le temps prendre soin de tout le monde… sauf de toi.
        </p>
    </div>
    <div className="text-center mt-5 mb-5">
        <button className="uppercase bg-pink-500 text-white hover:bg-yellow-500 p-3">
            Mon accompagnement peut t'aider
        </button>
    </div>
</div>
</div>
           </div>*/}

