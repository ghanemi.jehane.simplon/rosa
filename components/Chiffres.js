import React from 'react'

function Chiffres() {
    return (
        <div>
        <div className=" flex flex-col text-center mt-10 sm:flex-row sm:justify-evenly ">
            <div className="mb-5">
                <p className="text-7xl text-purpleRosa font-bold">30</p>
                <p className="text-2xl">Conférences nationales</p>
            </div>
            <div className="mb-5" >
                <p className="text-7xl  text-purpleRosa font-bold">600</p>
                <p className="text-2xl">Client.e.s satisfait.e.s</p>
            </div>
            <div className="mb-5">
                <p className="text-7xl text-purpleRosa font-bold">99%</p>
                <p className="text-2xl">de recommandations</p>
            </div>

            
        </div>
        <div className="mb-5 text-center">
                <p className="text-black text-3xl font-bold" >"N'attendez pas votre place, créez-la!"</p>
            </div>
        </div>
    )
}

export default Chiffres
