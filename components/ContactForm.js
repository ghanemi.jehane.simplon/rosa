import React, { useState } from 'react'
import { useForm } from 'react-hook-form';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.min.css';
import emailjs from 'emailjs-com';
import Link from 'next/link';

function ContactForm() {
    const [submitted, setSubmitted] = useState(false)
    const {
        register,
        handleSubmit,
        reset,
        formState: { errors }
    } = useForm();
    const toastifySuccess = () => {
        toast('Formulaire envoyé !', {
            position: 'bottom-right',
            autoClose: 5000,
            hideProgressBar: true,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: false,
            className: 'submit-feedback success',
            toastId: 'notifyToast'
        });
    };
    const onSubmit = async (data) => {
        const { name, email, firstname, message } = data;
        try {
            const templateParams = {
                name,
                firstname,
                email,
                message
            };
            await emailjs.send(
                process.env.NEXT_PUBLIC_SERVICE_ID,
                process.env.NEXT_PUBLIC_TEMPLATE_ID,
                templateParams,
                process.env.NEXT_PUBLIC_USER_ID
            );
            reset();
            setSubmitted(true)
            toastifySuccess()
        } catch (e) {
            console.log(e);
        }
    };
    return (
<div className=" mt-40">
<section className="text-center text-lg bg-white ">
                <p className="text-center  break-all">Si tu as des questions au sujet de mes services 
                 </p>
                 <p>ou si tu souhaites avancer sur ton épanouissement et ton impact professionnels,</p>
                 <p>je t’invite à faire une demande de séance découverte avec moi.</p>
                {/* Ajoutez lien calendly */}
                <Link href="https://calendly.com/ghozali-r">
                    <a target="_blank">
                    <button className="bg-purpleRosa mt-5 p-3 text-white uppercase justify-center hover:bg-yellow-500">rendez-vous visio</button>
                    </a>
                </Link>
            </section>
{submitted ? (

<div class="w-full bg-green-500 text-xl text-white text- mt-10">
<div class="container flex items-center justify-between px-6 py-4 mx-auto">
    <div class="flex">
        <svg viewBox="0 0 40 40" class="w-6 h-6 fill-current">
            <path d="M20 3.33331C10.8 3.33331 3.33337 10.8 3.33337 20C3.33337 29.2 10.8 36.6666 20 36.6666C29.2 36.6666 36.6667 29.2 36.6667 20C36.6667 10.8 29.2 3.33331 20 3.33331ZM16.6667 28.3333L8.33337 20L10.6834 17.65L16.6667 23.6166L29.3167 10.9666L31.6667 13.3333L16.6667 28.3333Z"></path>
        </svg>

        <p class="mx-3">Nous avons bien reçu votre message, merci à bientôt ! </p>
    </div>

    <button class="p-1 transition-colors duration-200 transform rounded-md hover:bg-opacity-25 hover:bg-gray-600 focus:outline-none">
        <svg class="w-5 h-5" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path d="M6 18L18 6M6 6L18 18" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"/>
        </svg>
    </button>
</div>
</div>


) : (


        <div class="bg-white rounded-lg shadow lg:h-full my-40 sm:max-w-md sm:w-full sm:mx-auto sm:overflow-hidden ">
            
            <div class="px-4 py-8 sm:px-10">
                <div class=" mt-6">
                    <div class=" flex justify-center text-sm leading-5">
                        <span class="px-2 text-2xl font-semibold bg-white text-purpleRosa">
                            Contactez moi
                        </span>
                    </div>
                </div>
                <form id='contact-form' onSubmit={handleSubmit(onSubmit)} noValidate>
                    <div class="mt-6">
                        <div class="w-full space-y-6">
                            <div class="w-full">
                                <div class="  ">
                                    <label htmlFor="name">Nom 
                                    <input name="name" type="text" {...register('name', { required: true })} id="search-form-name" class="rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Nom" />
                                    </label>
                                   
                                    {errors.name && <span className='errorMessage'>{errors.name.message}</span>}                              
                               </div>
                            </div>
                            <div class="w-full">
                                <div class="  ">
                                    <label htmlFor="firstname">Prénom 
                                    <input name="firstname" type="text" {...register('firstname', { required: true })} id="search-form-firstname" class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Prénom" />
                                    </label>
                                   
                                    {errors.name && <span className='errorMessage'>{errors.firstname.message}</span>}                              
                                </div>
                            </div>
                            <div class="w-full">
                                <div class="  ">
                                    <label htmlFor="email">Email 
                                    <input name="email" type="email"{...register('email', { required: true, pattern: /^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/ })} id="search-form-email" class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Email" />
                                    </label>
                                   
                                    {errors.email && (
                <span className='errorMessage'>Please enter a valid email address</span>
              )}
                              </div>
                              
                            </div>
                            <div class="w-full">
                                <div class="  ">
                                    <label htmlFor="message">Message 
                                    <textarea name="message" {...register('message', {
                                        required: true
                                    })} class=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" placeholder="Message" />
                                    </label>
                                   
              {errors.message && <span className='errorMessage'>Please enter a message</span>}                           
                                </div>
                            </div>
                            <div>
                                <span class="block w-full rounded-md shadow-sm">
                                    <button type="submit" class="py-2 px-4 transition duration-150 ease-in-out bg-purpleRosa text-white hover:scale-100  w-full text-center text-base font-semibold shadow-md rounded-lg ">
                                        Envoyer
                                    </button>
                                </span>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        )}
        <ToastContainer/>
        </div>
    )
}

export default ContactForm
