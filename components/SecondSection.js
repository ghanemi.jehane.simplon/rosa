import React from 'react'

function SecondSection() {
    return (
        <div className="flex-col lg:flex xl:flex lg:bg-second xl:bg-second lg:bg-cover xl:bg-cover lg:h-screen ">
            <div className="ml-5 mr-5 lg:bg-white lg:w-1/2 md:mt-20 md:px-5">
            <div className="px-4 py-16 mx-auto sm:max-w-xl md:max-w-full lg:max-w-screen-xl md:px-24 lg:px-8 lg:py-20">
      <div className="max-w-xl mb-10 md:mx-auto sm:text-center lg:max-w-2xl md:mb-12">
        <div>
          <p className="inline-block px-3 py-px mb-4 text-xs font-semibold tracking-wider text-blueRosa uppercase rounded-full bg-teal-accent-400">
            Just do it
          </p>
        </div>
        <h2 className="max-w-lg mb-6 font-sans text-3xl font-bold leading-none tracking-tight text-gray-900 sm:text-4xl md:mx-auto">
          <span className="relative inline-block">
            <svg
              viewBox="0 0 52 24"
              fill="currentColor"
              className="absolute top-0 left-0 z-0 hidden w-32 -mt-8 -ml-20 text-blueRosa lg:w-32 lg:-ml-28 lg:-mt-10 sm:block"
            >
              <defs>
                <pattern
                  id="a0b40128-6963-4319-aeeb-471e92fa2d74"
                  x="0"
                  y="0"
                  width=".135"
                  height=".30"
                >
                  <circle cx="1" cy="1" r=".7" />
                </pattern>
              </defs>
              <rect
                fill="url(#a0b40128-6963-4319-aeeb-471e92fa2d74)"
                width="52"
                height="24"
              />
            </svg>
            <span className="relative"> Alors</span>
          </span>{' '}
          si tu es prête à t'investir pour que le travail que nous accomplirons ensemble, te permette de:
        </h2>
      </div>
      <div className="max-w-lg space-y-3 sm:mx-auto lg:max-w-xl">
        <div className="flex items-center p-2 duration-300 transform border rounded shadow hover:scale-105 sm:hover:scale-110">
          <div className="mr-2">
            <svg
              className="w-6 h-6 text-purpleRosa sm:w-8 sm:h-8"
              stroke="currentColor"
              viewBox="0 0 52 52"
            >
              <polygon
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="none"
                points="29 13 14 29 25 29 23 39 38 23 27 23"
              />
            </svg>
          </div>
          <span className="text-gray-800">
          Récupérer la confiance en toi, et l'estime que tu as pour toi.
          </span>
        </div>
        <div className="flex items-center p-2 duration-300 transform border rounded shadow hover:scale-105 sm:hover:scale-110">
          <div className="mr-2">
            <svg
              className="w-6 h-6 text-purpleRosa sm:w-8 sm:h-8"
              stroke="currentColor"
              viewBox="0 0 52 52"
            >
              <polygon
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="none"
                points="29 13 14 29 25 29 23 39 38 23 27 23"
              />
            </svg>
          </div>
          <span className="text-gray-800">Découvrir les mécanismes de ton propre fonctionnement pour enfin, reprendre les commandes de ta vie</span>
        </div>
        <div className="flex items-center p-2 duration-300 transform border rounded shadow hover:scale-105 sm:hover:scale-110">
          <div className="mr-2">
            <svg
              className="w-6 h-6 text-purpleRosa sm:w-8 sm:h-8"
              stroke="currentColor"
              viewBox="0 0 52 52"
            >
              <polygon
                strokeWidth="3"
                strokeLinecap="round"
                strokeLinejoin="round"
                fill="none"
                points="29 13 14 29 25 29 23 39 38 23 27 23"
              />
            </svg>
          </div>
          <span className="text-gray-800">Puisez dans tes ressources, pour te donner la force de vivre la vie que tu souhaites</span>
        </div>

      </div>
    </div>
    <a href="/contact"className="flex justify-center">
    <button className="uppercase bg-purpleRosa text-center text-white hover:bg-blueRosa p-3 mb-5">
            Ne réfléchis plus réserves ta séance découverte !
        </button>
    </a>
  



            </div>

        </div>
    )
}

export default SecondSection
