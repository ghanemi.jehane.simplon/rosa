import React from 'react'

function NewsLetter() {
    return (
        <div className="bg-purpleRosa mt-30 mb-30 p-10 justify-center">
            <div className="text-white text-3xl text-center font-serif font-semibold">
                <h2>Tu veux être actrice de ta propre réussite ?
                    Je t’offre de l’inspiration pour construire ton parcours à ton image !</h2>
            </div>
            <div className="flex flex-col justify-center space-y-4  mt-5 mb-10 p-10 lg:space-x-4 lg:space-y-0 sm:flex-row">
                <input type="text" placeholder="Ton prénom" className="px-4"></input>
                <input type="email" placeholder="Ton email préféré" className="px-4"></input>
                <button className="uppercase py-3 px-2 bg-white hover:bg-blueRosa hover:text-white">Rejoindre mon club privé !</button>
            </div>
        </div>
    )
}

export default NewsLetter
