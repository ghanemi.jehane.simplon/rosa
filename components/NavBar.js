import React, { useState } from 'react'

function NavBar() {
    const [showLinks, setShowLinks] = useState(false);
    const handleShowLinks = ()=>{
        setShowLinks(!showLinks)
    }
    return (
        <nav className="mb-5" className={` ${showLinks ? "show-nav" : "hide-nav"} `}>
            <div className="flex flex-col md:flex-row justify-evenly mt-5 navbar_links">
                <a href="/" className="uppercase border-b-8 hover:border-purpleRosa cursor-pointer navbar_link">Accueil</a>
                <a href="/about" className="uppercase border-b-8 hover:border-purpleRosa cursor-pointer navbar_link">Me connaître</a>
                <a href="/coaching" className="uppercase border-b-8 hover:border-purpleRosa cursor-pointer navbar_link">Coaching</a>
                <a href="/news" className="uppercase border-b-8 hover:border-purpleRosa cursor-pointer navbar_link">Blog</a>
                <a href="/contact" className="uppercase border-b-8 hover:border-purpleRosa cursor-pointer navbar_link">Contact</a>
            
            </div>
            <button className="navbar_burger" onClick={handleShowLinks}>
                <span className="burger-bar"></span>
            </button>
        </nav>
    )
}

export default NavBar
