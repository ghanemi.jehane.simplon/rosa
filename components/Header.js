import React from 'react'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"; 
import {faSearch } from '@fortawesome/free-solid-svg-icons';
import { faFacebook, faInstagram, faLinkedin, faPinterest, faYoutube } from '@fortawesome/free-brands-svg-icons';
import Image from 'next/image'

function Header() {
    return (
        <div>
        <div className="flex space-x-8 my-5 justify-center mt-3 text-blueRosa ">
            
            <a target="_blank" href="https://www.facebook.com/wardya.sekkour.3"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faFacebook} style={{width:'25px'}}/></a>
            <a target="_blank" href="https://www.linkedin.com/in/rosa-ghozali"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faLinkedin} style={{width:'25px'}}/></a>
           <a target="_blank" href="https://www.instagram.com/les_ateliers_de_rosa/"><FontAwesomeIcon className="cursor-pointer hover:text-purpleRosa" icon={faInstagram} style={{width:'25px'}}/></a> 
           
          
        </div>

        <div className="flex justify-center">
            <a href="/">
            <Image src="/image0.png" className="rounded-full cursor-pointer" alt="logo" width={150} height={150}/>
            </a>
            
        </div>
      
        </div>
    )
}

export default Header
