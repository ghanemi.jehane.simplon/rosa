import React from 'react'
import { Pagination, Autoplay } from 'swiper';
import reviews from '../data/reviews.json'
import { Swiper, SwiperSlide } from 'swiper/react';




function Slider() {

  return (
    <div className="my-20 bg-beige">
      <Swiper
        modules={[Autoplay, Pagination]}

        spaceBetween={50}
        slidesPerView={'auto'}
        centeredSlides={true}
        autoplay={{
          delay: 3000,
          disableOnInteraction: false,
        }}
        pagination={{ clickable: true }}
        loop={true}
        className="p-3 max-w-screen-lg items-start">

        {reviews.map((review) =>
          <SwiperSlide>
            <div class="relative block h-96  p-8 pb-24 border-t-4 border-purpleRosa rounded-sm shadow-xl">
              <h5 class="text-4xl font-medium">{review.name}</h5>
              <p class="mt-4 text-lg items-start font-medium text-gray-500">
                {review.review}
              </p>

              <span class="absolute bottom-8 right-8">
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  class="w-10 h-10 text-purpleRosa"
                  fill="none"
                  viewBox="0 0 24 24"
                  stroke="currentColor"
                >
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 10V3L4 14h7v7l9-11h-7z" />
                </svg>
              </span>
            </div>

          </SwiperSlide>
        )}

      </Swiper>

    </div>

  )
}

export default Slider
